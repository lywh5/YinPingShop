(function(){
    function w() {
    // document.documentElement : 获取html节点的dom对象
    var r = document.documentElement;

    //Element.getBoundingClientRect()方法返回元素的大小及其相对于视口的位置。

    //返回浏览器窗口的宽度
    var a = r.getBoundingClientRect().width;

        if (a > 750 ){
            a = 750;
        } 
        rem = a / 7.5;
        r.style.fontSize = rem + "px"
    }
    var t;
    w();
    
     window.addEventListener("resize", function() {
        clearTimeout(t);
        t = setTimeout(w, 300)
    }, false);
})();
